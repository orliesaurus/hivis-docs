# Onboarding Guide - hivis.io

First of all, thank you for joining us for our early BETA release. We’ve built this product because we believe things are better explained when data meets visualizations, and aiming for high-visibility (this is where the name comes from) is the ultimate goal of many companies.

## Table of Contents

- [Overview](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#overview)
- [Accessing the platform](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#accessing-the-platform)
- [First look at the dashboard](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#first-look-at-the-dashboard)
- [Creating your first template](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#creating-your-first-template)
- [Your first API call](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#your-first-api-call)
 * [Instant Rendering](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#instant-rendering)
 * [Cached Rendering](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#cached-rendering)
- [FAQ](https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#faq)

## Overview

Our platform lets users generate images from templates dynamically using HTML/CSS and the [Liquid templating language](http://shopify.github.io/liquid/) created by Shopify. 

The platform is open to anyone and signing up is **free**. We are not charging at this moment for usage, template re-generation, image caching or customer support.

However, we are definitely thinking about a pricing structure that makes sense. 

As of today we guarantee a free-tier for anyone and that will continue for the foreseeable future.

Our infrastructure is built around the Google Cloud engine and its Firebase offering: It’s is resilient, scalable and responsive. 

## Accessing the platform

Signing up is done by visiting the hivis.io signup [page located here](https://hivis.io/sign-up). We use Firebase’s authentication and take your privacy seriously. Passwords are safely kept private and even we can’t see them.

## First look at the dashboard

Login to your dashboard clicking [here](https://hivis.io/login)

You will be greeted with a screen looking like this

![image alt text](image_0.png)

*Fig 1. The dashboard*

Let’s take a minute to locate a few important parts of this dashboard.


At the top of your screen all your **Account Details** are listed in the following order:

* **UserID**: Your unique UserID which is used identify your account and workspace

* **Email**: The email for the account, use to authenticate your account and for us to contact you (well, eventually we will!)

* **Secret**: This is a secret key we use to generate your signature, treat it like an API Key.

Below the account details, you will find information regarding your **Templates**.

* **Template**: A template is defined by you. It can be any valid combination of HTML (including IMG/SVG tags), CSS and Liquid that can be rendered into the DOM (Document Object Model). The DOM is then rendered into an image is the output of of every API call. 

* **Variables**: Highlighted in yellow, these are the parameters that are passed via an API call to the template, parsed and rendered at render time. 

* **Template Id**:This string identifies your template in your workspace. Each Template Id is unique to a User.

<table>
  <tr>
    <td>⚠️</td>
    <td>A signature is used when you request a template to be rendered by our API. It in fact allows our API to authorize the rendering of a template into an image by identifying the HTTP request as authorized</td>
  </tr>
</table>


## First look at the template editor

![image alt text](image_1.png)

*Fig 2. The template editor*

As mentioned earlier the template editor lets anyone create a DOM that gets rendered into an image by our API.

The editor is divided in three parts:

* On the left hand side you can write code that will be rendered by the API. You can use any combination of HTML, CSS and Liquid.

* On the right hand side you can preview, after saving all your variables and hitting the Preview button, the final rendering of your template

* Also on the right hand side, but on the lower part, an area where you can define API parameters and respective values. These parameters will be passed to the API, injected into the template and rendered as an image. Such variables can be numbers or strings.

<table>
  <tr>
    <td>⚠️</td>
    <td>Don’t forget to encode string variables when you send them over as an API call. For example: 
<code>hello world!!!</code>
becomes
<code>hello+world%21%21%21</code>
</td>
  </tr>
</table>


## Creating your first template

Let’s create a template from scratch to get familiar with the environment.

First using style tags we set the margin and padding on the body to 0.

Next we create a container using a div, assign it an id of *container* and set the background color in the style tags to `{{background_color}}`

As a best practice, we define all variables on the right hand side under the preview pane but we initialize them with default values in code.

Going ahead we type bg on the left cell and a value of red on the right cell, we press the "+" button or press enter to ensure the variable is created correctly.

Make sure there’s always an empty field under your last variables, take a look at the picture to see what I mean, that is a little gotcha to ensure the variable has been correctly entered in the template.

![image alt text](image_2.png)

Using Liquid we type: `{% assign background_color = bg | default: "purple" %}`

This will assign the value from the API parsed as *bg* to *background_color*, if no value is parsed because the parameter is missing the default will be "purple"

Inside the container we create an image tag and assign it an id of *animal* and it’s src attribute to your favorite animal picture or in my case a placeholder image ([https://placehold.it/250](https://placehold.it/250))

So far the code should look like this:

`{% assign background_color = bg | default: "purple" %}`

```
<style>

  html, body { margin: 0;

    padding: 0;

  }

  #container {

    background-color: {{background_color}};

  }

</style>

<div id="container">

  <img id="animal" src="https://placehold.it/250">

</div>
```

Next we create a div with an id named *greeting *under the image tag and inside it we write:

`Hello {{animal_name}}`

We then create a variable called *name* in the right pane under the preview and assign it a value of Billy

We also use liquid language to define a default variable

 `{% assign animal_name = name | default: "Billy" %}`

Finally we add the style property of `text-align: center;` to the #container

Hit the save button and the preview button to see the final result.

## Your first API Call

There are 2 types of RESTFul API call supported by hivis.io: An instant-rendering and a pre-rendering call:

### Instant rendering

* Instant rendering is performed with a GET and returns an image on the fly.

The format of the request is https://hivis.io/api/**:UserId**/**:TemplateId**/**:Signature**?:**Variables**

Where the variables are the ones you defined in your template followed by a few system variables:
**_height:** the canvas’ height size in pixels
**_width:** the canvas’ width size in pixels
**_density:** the density multiplier, default is 1 if you omit it. (i.e. for retina-display is 2)

#### Example

**_Open the following URL_**

<table>
  <tr>
    <td>📦</td>
    <td>https://hivis.io/api/RjzqrK914ZV5egtRbgisrdmxDlb2/ShEkS2icblZLKtVYeBpU/7ac26541b3a092924028581b8b13254e79081f1d?_height=280&_width=700&date=2018-05-03&text=Update%20%60mailgun-js%60%20to%20fix%20a%20vulnerability&githubUrl=orliesaurus%2Fnodemailer-mailgun-transport&starCount=432&lang=JavaScript</td>
  </tr>
</table>





### Cached rendering

The second type of request is received, executed and cached by the server. Great for background processing. This way you can send us a lot of requests at once and not receive the image but instead a URL, that you can parse out and store for future use.

* Such "pre-rendering" is performed through a POST and returns a URL

    * POST the request such as:

#### Example

<table>
  <tr>
    <td>📦</td>
    <td>curl -X POST https://hivis.io/api/:UserID/:TemplateID \ <br>
-H "content-type: application/json" \ <br>
-d '{ \ <br>
    "secret": "53cr37GcZCtD", \ <br>
    "params": {"var1": "value", "var2":"value"}, \ <br>
    "options": {"_width": 300, "_height": 300} \ <br>
   }'</td>
  </tr>
</table>


## FAQ

### Account verification/2FA

There currently aren’t any account verification steps. Once you provide your email and password you’re ready to go. We do not provide 2-Factor Authentication at this moment.

### Password reset

At this moment, if you forget your password you can’t reset it. Please make sure you don’t lose it!

