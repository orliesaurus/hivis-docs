<h1 id="onboardingguidehivisio">Onboarding Guide - hivis.io</h1>
<p>First of all, thank you for joining us for our early BETA release. We’ve built this product because we believe things are better explained when data meets visualizations, and aiming for high-visibility (this is where the name comes from) is the ultimate goal of many companies.</p>
<h2 id="tableofcontents">Table of Contents</h2>
<ul>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#overview">Overview</a></li>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#accessing-the-platform">Accessing the platform</a></li>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#first-look-at-the-dashboard">First look at the dashboard</a></li>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#creating-your-first-template">Creating your first template</a></li>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#your-first-api-call">Your first API call</a></li>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#instant-rendering">Instant Rendering</a></li>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#cached-rendering">Cached Rendering</a></li>
<li><a href="https://gitlab.com/orliesaurus/hivis-docs/blob/master/DOCS.md#faq">FAQ</a></li>
</ul>
<h2 id="overview">Overview</h2>
<p>Our platform lets users generate images from templates dynamically using HTML/CSS and the <a href="http://shopify.github.io/liquid/">Liquid templating language</a> created by Shopify. </p>
<p>The platform is open to anyone and signing up is <strong>free</strong>. We are not charging at this moment for usage, template re-generation, image caching or customer support.</p>
<p>However, we are definitely thinking about a pricing structure that makes sense. </p>
<p>As of today we guarantee a free-tier for anyone and that will continue for the foreseeable future.</p>
<p>Our infrastructure is built around the Google Cloud engine and its Firebase offering: It’s is resilient, scalable and responsive. </p>
<h2 id="accessingtheplatform">Accessing the platform</h2>
<p>Signing up is done by visiting the hivis.io signup <a href="https://hivis.io/sign-up">page located here</a>. We use Firebase’s authentication and take your privacy seriously. Passwords are safely kept private and even we can’t see them.</p>
<h2 id="firstlookatthedashboard">First look at the dashboard</h2>
<p>Login to your dashboard clicking <a href="https://hivis.io/login">here</a></p>
<p>You will be greeted with a screen looking like this</p>
<p><img src="image_0.png" alt="image alt text" /></p>
<p><em>Fig 1. The dashboard</em></p>
<p>Let’s take a minute to locate a few important parts of this dashboard.</p>
<p>At the top of your screen all your <strong>Account Details</strong> are listed in the following order:</p>
<ul>
<li><p><strong>UserID</strong>: Your unique UserID which is used identify your account and workspace</p></li>
<li><p><strong>Email</strong>: The email for the account, use to authenticate your account and for us to contact you (well, eventually we will!)</p></li>
<li><p><strong>Secret</strong>: This is a secret key we use to generate your signature, treat it like an API Key.</p></li>
</ul>
<p>Below the account details, you will find information regarding your <strong>Templates</strong>.</p>
<ul>
<li><p><strong>Template</strong>: A template is defined by you. It can be any valid combination of HTML (including IMG/SVG tags), CSS and Liquid that can be rendered into the DOM (Document Object Model). The DOM is then rendered into an image is the output of of every API call. </p></li>
<li><p><strong>Variables</strong>: Highlighted in yellow, these are the parameters that are passed via an API call to the template, parsed and rendered at render time. </p></li>
<li><p><strong>Template Id</strong>:This string identifies your template in your workspace. Each Template Id is unique to a User.</p></li>
</ul>
<table>
  <tr>
    <td>⚠️</td>
    <td>A signature is used when you request a template to be rendered by our API. It in fact allows our API to authorize the rendering of a template into an image by identifying the HTTP request as authorized</td>
  </tr>
</table>
<h2 id="firstlookatthetemplateeditor">First look at the template editor</h2>
<p><img src="image_1.png" alt="image alt text" /></p>
<p><em>Fig 2. The template editor</em></p>
<p>As mentioned earlier the template editor lets anyone create a DOM that gets rendered into an image by our API.</p>
<p>The editor is divided in three parts:</p>
<ul>
<li><p>On the left hand side you can write code that will be rendered by the API. You can use any combination of HTML, CSS and Liquid.</p></li>
<li><p>On the right hand side you can preview, after saving all your variables and hitting the Preview button, the final rendering of your template</p></li>
<li><p>Also on the right hand side, but on the lower part, an area where you can define API parameters and respective values. These parameters will be passed to the API, injected into the template and rendered as an image. Such variables can be numbers or strings.</p></li>
</ul>
<table>
  <tr>
    <td>⚠️</td>
    <td>Don’t forget to encode string variables when you send them over as an API call. For example: 
<code>hello world!!!</code>
becomes
<code>hello+world%21%21%21</code>
</td>
  </tr>
</table>
<h2 id="creatingyourfirsttemplate">Creating your first template</h2>
<p>Let’s create a template from scratch to get familiar with the environment.</p>
<p>First using style tags we set the margin and padding on the body to 0.</p>
<p>Next we create a container using a div, assign it an id of <em>container</em> and set the background color in the style tags to <code>{{background_color}}</code></p>
<p>As a best practice, we define all variables on the right hand side under the preview pane but we initialize them with default values in code.</p>
<p>Going ahead we type bg on the left cell and a value of red on the right cell, we press the "+" button or press enter to ensure the variable is created correctly.</p>
<p>Make sure there’s always an empty field under your last variables, take a look at the picture to see what I mean, that is a little gotcha to ensure the variable has been correctly entered in the template.</p>
<p><img src="image_2.png" alt="image alt text" /></p>
<p>Using Liquid we type: <code>{% assign background_color = bg | default: "purple" %}</code></p>
<p>This will assign the value from the API parsed as <em>bg</em> to <em>background_color</em>, if no value is parsed because the parameter is missing the default will be "purple"</p>
<p>Inside the container we create an image tag and assign it an id of <em>animal</em> and it’s src attribute to your favorite animal picture or in my case a placeholder image (<a href="https://placehold.it/250">https://placehold.it/250</a>)</p>
<p>So far the code should look like this:</p>
<p><code>{% assign background_color = bg | default: "purple" %}</code></p>
<pre><code>&lt;style&gt;

  html, body { margin: 0;

    padding: 0;

  }

  #container {

    background-color: {{background_color}};

  }

&lt;/style&gt;

&lt;div id="container"&gt;

  &lt;img id="animal" src="https://placehold.it/250"&gt;

&lt;/div&gt;
</code></pre>
<p>Next we create a div with an id named *greeting *under the image tag and inside it we write:</p>
<p><code>Hello {{animal_name}}</code></p>
<p>We then create a variable called <em>name</em> in the right pane under the preview and assign it a value of Billy</p>
<p>We also use liquid language to define a default variable</p>
<p><code>{% assign animal_name = name | default: "Billy" %}</code></p>
<p>Finally we add the style property of <code>text-align: center;</code> to the #container</p>
<p>Hit the save button and the preview button to see the final result.</p>
<h2 id="yourfirstapicall">Your first API Call</h2>
<p>There are 2 types of RESTFul API call supported by hivis.io: An instant-rendering and a pre-rendering call:</p>
<h3 id="instantrendering">Instant rendering</h3>
<ul>
<li>Instant rendering is performed with a GET and returns an image on the fly.</li>
</ul>
<p>The format of the request is https://hivis.io/api/<strong>:UserId</strong>/<strong>:TemplateId</strong>/<strong>:Signature</strong>?:<strong>Variables</strong></p>
<p>Where the variables are the ones you defined in your template followed by a few system variables:
<strong><em>height:</strong> the canvas’ height size in pixels
<strong></em>width:</strong> the canvas’ width size in pixels
<strong>_density:</strong> the density multiplier, default is 1 if you omit it. (i.e. for retina-display is 2)</p>
<h4 id="example">Example</h4>
<p><strong><em>Open the following URL</em></strong></p>
<table>
  <tr>
    <td>📦</td>
    <td>https://hivis.io/api/RjzqrK914ZV5egtRbgisrdmxDlb2/ShEkS2icblZLKtVYeBpU/7ac26541b3a092924028581b8b13254e79081f1d?_height=280&_width=700&date=2018-05-03&text=Update%20%60mailgun-js%60%20to%20fix%20a%20vulnerability&githubUrl=orliesaurus%2Fnodemailer-mailgun-transport&starCount=432&lang=JavaScript</td>
  </tr>
</table>
<h3 id="cachedrendering">Cached rendering</h3>
<p>The second type of request is received, executed and cached by the server. Great for background processing. This way you can send us a lot of requests at once and not receive the image but instead a URL, that you can parse out and store for future use.</p>
<ul>
<li><p>Such "pre-rendering" is performed through a POST and returns a URL</p>
<ul>
<li>POST the request such as:</li></ul></li>
</ul>
<h4 id="example-1">Example</h4>
<table>
  <tr>
    <td>📦</td>
    <td>curl -X POST https://hivis.io/api/:UserID/:TemplateID \ <br>
-H "content-type: application/json" \ <br>
-d '{ \ <br>
    "secret": "53cr37GcZCtD", \ <br>
    "params": {"var1": "value", "var2":"value"}, \ <br>
    "options": {"_width": 300, "_height": 300} \ <br>
   }'</td>
  </tr>
</table>
<h2 id="faq">FAQ</h2>
<h3 id="accountverification2fa">Account verification/2FA</h3>
<p>There currently aren’t any account verification steps. Once you provide your email and password you’re ready to go. We do not provide 2-Factor Authentication at this moment.</p>
<h3 id="passwordreset">Password reset</h3>
<p>At this moment, if you forget your password you can’t reset it. Please make sure you don’t lose it!</p>
