const fs = require('fs')
const showdown = require('showdown')
const converter = new showdown.Converter()

markdownText = fs.readFileSync('DOCS.md').toString()
console.log(converter.makeHtml(markdownText))
