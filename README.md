# hivis.io Developer Onboarding Documentation

This repo contains the documentation used to help developers onboard into the
platform. This is a living document meaning it will be updated as often as
possible to ensure all changes to the APIs and to the platform itself will be
consistently documented.

# Contributions
Please open a PR if you believe something is unclear and clearly explain your
issue and I will make sure to get it fixed or comment on your PR.
